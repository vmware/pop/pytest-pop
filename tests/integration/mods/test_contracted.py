import pop.contract
import pytest


@pytest.fixture(scope="module", name="hub")
def my_hub(hub):
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    yield hub


def test_hub_contract(hub):
    assert hub.mods.testing.echo("foo") == "contract foo"


def test_contract_hub_contract(contract_hub):
    assert isinstance(contract_hub.mods.testing.echo, pop.contract.Contracted)


@pytest.mark.asyncio
async def test_async_echo(hub, contract_hub):
    val = "foo"
    expected = "async contract " + val
    assert await hub.mods.testing.contracted_async_echo(val) == expected

    contract_hub.mods.testing.contracted_async_echo.func.return_value = val
    assert (
        await contract_hub.mods.testing.contracted_async_echo(val + "change")
        == expected
    )


def test_contract_hub_inspect(contract_hub):
    # demo ways that we can inspect the contract system
    assert len(contract_hub.mods.testing.echo.contracts) == 1
    assert "call_signature_func" in dir(contract_hub.mods.testing.echo.contracts[0])


def test_contract_hub_modify(contract_hub, hub):
    # modifying the contract
    contract_hub.mods.testing.echo.func.return_value = "bar"
    assert contract_hub.mods.testing.echo("foo") == "contract bar"
    contract_hub.mods.testing.echo.contract_functions["call"] = []
    assert contract_hub.mods.testing.echo("foo") == "bar"

    # verify that modification didn't mess with the real hub:
    assert hub.mods.testing.echo("foo") == "contract foo"
