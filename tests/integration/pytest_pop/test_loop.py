import pytest


@pytest.mark.asyncio
async def test_asyncio(hub):
    assert not hub.pop.Loop.is_closed()
